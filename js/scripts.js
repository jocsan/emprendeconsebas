$(window).scroll(function () {
    if ($(window).scrollTop() >= 75) {
        $('.timer-container').addClass('onscroll');
    } else {
        $('.timer-container').removeClass('onscroll');
    }
});

const items = document.querySelectorAll(".accordion a");

function toggleAccordion() {
    this.classList.toggle('active');
    this.nextElementSibling.classList.toggle('active');
}

items.forEach(item => item.addEventListener('click', toggleAccordion));