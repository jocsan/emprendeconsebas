<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <title>Domina Instagram</title>

 <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <link rel="stylesheet" href="css/font-awesome.min.css">
  <link href="./css/bootstrap.min.css" rel="stylesheet">
  <link href="./css/mdb.min.css" rel="stylesheet">
  <link href="./css/style.css" rel="stylesheet">
</head>

<body>
  <div class="container-fluid px-0 position-relative">
    <div class="timer-container w-100" id="timer">
        <div class="row align-items-center">
            <div class="col-12 col-lg-6 timer-texto">
                <p class="mb-0">⏱ Oferta única por el día de hoy, ¡aprovecha ya!</p>
            </div>
            <div class="col-12 col-lg-6 timer-df">
              <div class="row justify-content-center cd100">
                <div class="col-2 text-center">
                  <p class="h1 h1-responsive hours text-white mb-0">00</p>
                  <p class=" text-white mb-0">Hr</p>
                </div>

                <div class="col-2 text-center">
                  <p class="h1 h1-responsive minutes text-white mb-0">00</p>
                  <p class=" text-white mb-0">Min</p>
                </div>

                <div class="col-2 text-center">
                  <p class="h1 h1-responsive seconds text-white mb-0">00</p>
                  <p class=" text-white mb-0">Seg</p>
                </div>
              </div>
            </div>
        </div>
    </div>
  </div>
  <a href="https://wa.me/5492392405937?text=Hola%2C%20me%20interesa%20mucho%20el%20curso%20de%20instagram%20%F0%9F%93%B2%F0%9F%94%A5" class="float" target="_blank">
    <i class="fa fa-whatsapp my-float"></i>
  </a>

  <div class="py-5 mt-5" style="margin-left: 5%;margin-right: 5%;">
    <p class="h1 h1-responsive text-center font-weight-bold mb-4 red-text">¡Domina Instagram y consigue miles de
      seguidores!</p>
    <p class="h4 h4-responsive text-center hidden-phone">El curso que te llevará desde lo básico a lo avanzado para que seas un
      experto en Instagram</p>
      <p class="h4 h4-responsive text-center hidden-desktop" style="margin-bottom: -70px;">Mira el video <br><i class="fa fa-hand-o-down"></i></p>
      <br class="hidden-phone"><br class="hidden-phone"><br class="hidden-phone">
    <div class="row">
      <div class="col-12 col-md-8">
        <iframe style="height: 60vh;" poster="./img/moneyInstagram.png" src="https://player.vimeo.com/video/303895307" width="100%" height="500" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
      </div>
      <div class="col-12 col-md-4">
        <p class="h2 h2-responsive text-center red darken-1 p-2 text-white font-weight-bold mb-0">¡Ahorra $3780 solo por
          hoy!</p>
        <div class="container-fluid z-depth-1 pt-3 pb-2">
          <p><i class="fa fa-check-circle red-text" aria-hidden="true"></i> Crea, estructura y optimiza correctamente
            tu cuenta de instagram.</p>
          <p><i class="fa fa-check-circle red-text" aria-hidden="true"></i> Encuentra tu nicho rentable.</p>
          <p><i class="fa fa-check-circle red-text" aria-hidden="true"></i> Diseña imágenes de manera gratuita desde
            pc, laptop, tablet o celular.</p>
          <p><i class="fa fa-check-circle red-text" aria-hidden="true"></i> Enlaces a bancos de imágenes con millones
            de fotografías gratuitas.</p>
          <p><i class="fa fa-check-circle red-text" aria-hidden="true"></i> Genera interacción con tus seguidores.</p>
          <p><i class="fa fa-check-circle red-text" aria-hidden="true"></i> Publica en los mejores horarios.</p>
          <p><i class="fa fa-check-circle red-text" aria-hidden="true"></i> Domina el uso del hashtag.</p>
          <p><i class="fa fa-check-circle red-text" aria-hidden="true"></i> Analiza correctamente las estadísticas de
            tu cuenta.</p>
          <p><i class="fa fa-check-circle red-text" aria-hidden="true"></i> Consigue miles de seguidores.</p>
          <p><i class="fa fa-check-circle red-text" aria-hidden="true"></i> Monetiza tu cuenta de Instagram.</p>
          <p><i class="fa fa-check-circle red-text" aria-hidden="true"></i> Instagram ads, crea promociones
            extremadamente rentables.</p>
          <p><i class="fa fa-check-circle red-text" aria-hidden="true"></i> ¡Y mucho más!</p>
        </div>
        <a href="https://payment.hotmart.com/U9885844B?off=42y0n4h3&checkoutMode=10" target="_blank" class="btn btn-domain btn-block btn-lg mt-3 font-weight-bold btn-rounded">¡si! quiero dominar
          instagram</a>
      </div>
    </div>
  </div>

  <div class="container py-5">
    <p class="h2 h2-responsive text-center red-text font-weight-bold mb-0">¡Ahorra $3780 solo por hoy!</p>
    <p class="text-dark text-center font-weight-bold mb-5">* Bono de disponibilidad garantizada hasta la medianoche. A
      partir de entonces, nos reservamos el derechos de eliminarlos.</p>    

    <p class="h2 h2-responsive text-center red-text font-weight-bold mb-2">¿Por qué debo dominar Instagram ahora?</p>
    <p class="h2 h2-responsive text-center red-text font-weight-bold mb-0">¿Por qué es posible hacer dinero con
      Instagram?</p>

    <div class="row mt-5 align-items-center">
      <div class="col-12 col-md px-0">
        <div class="container-fluid z-depth-1 pt-3 pb-2">
          <p><i class="fa fa-check-circle red-text" aria-hidden="true"></i> Instagram tiene mas de 800 millones de
            usuarios.</p>
          <p><i class="fa fa-check-circle red-text" aria-hidden="true"></i> Tiene 80% mas interacción que Facebook.</p>
          <p><i class="fa fa-check-circle red-text" aria-hidden="true"></i> Tiene 95 millones de publicaciones diarias.</p>
          <p><i class="fa fa-check-circle red-text" aria-hidden="true"></i> Más de 500 millones de usuarios activos
            diariamente.</p>
          <p><i class="fa fa-check-circle red-text" aria-hidden="true"></i> Genera más de 1.5 billones de reacciones
            diariamente (likes y comentarios).</p>
          <p><i class="fa fa-check-circle red-text" aria-hidden="true"></i> Tiene un alcance orgánico muy potente.</p>
        </div>
      </div>
      <div class="col-12 col-md-7 px-0">
        <img src="./img/moneyInstagram.png" alt="¿Por qué debo dominar Instagram ahora?" class="img-fluid z-depth-2 d-block mx-auto rounded">
      </div>
    </div>
  </div>

  <div class="container py-5">
    <p class="h2 h2-responsive text-center p-2 red-text font-weight-bold">¿Por qué elegirnos?</p>

    <div class="row mt-5">
      <div class="col-12 col-md-3 text-center">
        <p class="red-text display-1"><i class="fa fa-check"></i></p>
        <p class="lead font-weight-bold mt-3 mb-1">Métodos Garantizados</p>
        <p>Todo lo que te enseñaremos es lo que hemos aplicado para llevar una cuenta de 0 a 760.000 seguidores.</p>
      </div>
      <div class="col-12 col-md-3 text-center">
        <p class="red-text display-1"><i class="fa fa-arrow-circle-up"></i></p>
        <p class="lead font-weight-bold mt-3 mb-1">Actualizaciones Gratis de por Vida</p>
        <p>Sabemos que las redes sociales cambian constantemente, por eso el curso se irá actualizando con nuevas
          estrategias y al acceder hoy gozaras de todas estas actualizaciones de manera gratuita.</p>
      </div>
      <div class="col-12 col-md-3 text-center">
        <p class="red-text display-1"><i class="fa fa-slideshare"></i></p>
        <p class="lead font-weight-bold mt-3 mb-1">Estará para Guiarte</p>
        <p class="mb-0">¿Tienes dudas?</p>
        <p class="mb-0">¿Necesitas algún consejo?</p>
        <p>Escribeme y estare encantado de guiarte y ayudarte a crecer de manera exponencial en tu cuenta.</p>
      </div>
      <div class="col-12 col-md-3 text-center">
        <p class="red-text display-1"><i class="fa fa-book"></i></p>
        <p class="lead font-weight-bold mt-3 mb-1">Aprendizaje Continuo</p>
        <p>Al adquirir hoy el curso tendrás acceso a todos los webinar que realizemos, que irán desde temas de
          emprendimiento hasta nuevas técnicas de redes sociales.</p>
      </div>
    </div>

    <div class="row mt-5 align-items-center">
      <div class="col-12 col-md-3">
        <img src="./img/garantia.png" alt="" class="img-fluid d-block mx-auto">
      </div>
      <div class="col-12 col-md">
        <p class="text-center">Esta es nuestra promesa: compre hoy 100% sin riesgos, tenemos una garantía de devolución
          de 7 días. Si por alguna razón no está satisfecho con el curso simplemente contáctese con nuestro amigable
          servicio de asistencia y se le dará un reembolso completo de inmediato.</p>
      </div>
      <div class="col-12 col-md-3">
        <img src="./img/garantia.png" alt="" class="img-fluid d-block mx-auto">
      </div>
    </div>

    <div class="text-center">
      <a href="https://payment.hotmart.com/U9885844B?off=42y0n4h3&checkoutMode=10" target="_blank" class="btn btn-domain btn-lg btn-rounded mt-5 font-weight-bold">registrate ahora</a>
    </div>
  </div>

  <div class="container py-5">
    <p class="h2 h2-responsive text-center red-text font-weight-bold mt-5">¿A quien va dirigido Instagram?</p>
    <div class="row mt-4 align-items-center">
      <div class="col-12 col-md px-0">
        <div class="container-fluid z-depth-1 pt-3 pb-2">
          <p><i class="fa fa-check-circle red-text" aria-hidden="true"></i> Dueños de negocios online o offline.</p>
          <p><i class="fa fa-check-circle red-text" aria-hidden="true"></i> Vendedores de productos físicos y
            digitales.</p>
          <p><i class="fa fa-check-circle red-text" aria-hidden="true"></i> Networkers y empresarios de todo tipo.</p>
          <p><i class="fa fa-check-circle red-text" aria-hidden="true"></i> Personas con negocios físicos.</p>
          <p><i class="fa fa-check-circle red-text" aria-hidden="true"></i> Emprendedores que quieran ganar dinero
            creando su imperio en Instagram.</p>
        </div>
      </div>
      <div class="col-12 col-md-7 px-0">
        <img src="./img/addressedInstagram.png" alt="¿A quien va dirigido Instagram?" class="img-fluid z-depth-2 d-block mx-auto rounded">
      </div>
    </div>
  </div>

  <div class="container py-5">
    <p class="h1 h1-responsive red-text text-center mb-5">¡Llévate totalmente gratis todos estos bonos el dia de hoy y ahorra $3780 dolares!</p>
    <p class="text-justify mb-3 font-weight-light"><i class="fa fa-check-circle red-text" aria-hidden="true"></i> Libro pdf como Ganar 50 000 dolares en redes sociales</p>
    <p class="text-justify mb-3 font-weight-light"><i class="fa fa-check-circle red-text" aria-hidden="true"></i> Cupón de 400$ para publicidad canjeable en @poemas_delibros_  @razonamientos__ y @emprendeconsebas</p>
    <p class="text-justify mb-3 font-weight-light"><i class="fa fa-check-circle red-text" aria-hidden="true"></i> Acceso por 6 meses a las clases privadas en vivo todas las semanas nuevas clases</p>
    <p class="text-justify font-weight-light"><i class="fa fa-check-circle red-text" aria-hidden="true"></i> Asesoria directa durante 6 meses</p>
  </div>

  <div class="container py-5">
    <p class="h2 h2-responsive text-center red-text font-weight-bold mt-5">Preguntas Frecuentes</p>
    <div class="accordion">
      <div class="accordion-item">
        <a>¿Es un único pago o tengo que pagar algo más?</a>
        <div class="content">
          <p>El pago es único, no hay mensualidades y tienes el acceso de por vida</p>
        </div>
      </div>
      <div class="accordion-item">
        <a>¿Cuando y donde se imparte el curso?</a>
        <div class="content">
          <p>El curso es 100% online y podrás comenzar a estudiar en unos pocos segundos</p>
        </div>
      </div>
      <div class="accordion-item">
        <a>¿Si tengo dudas me las pueden aclarar un tutor?</a>
        <div class="content">
          <p>Asi es, responderemos todas tus dudas, nuestro objetivo es que al terminar el curso puedas entender y dominar instagram en su totalidad.</p>
        </div>
      </div>
      <div class="accordion-item">
        <a>¿Qué metodos de pago estan disponible?</a>
        <div class="content">
          <p>Todas las tarjetas de credito o debito, paypal. En caso de desear hacer el pago con otro medio de pago simplemente comuníquenoslo en el chat.</p>
        </div>
      </div>
      <div class="accordion-item">
        <a>¿Puedo acceder al curso desde mi móvil o tablet?</a>
        <div class="content">
          <p>¡Claro que si! puedes acceder con cualquier dispositivo con acceso a internet.</p>
        </div>
      </div>
    </div>
  </div>

  <p class="h6 h6-responsive text-center">* Los resultados mostrados no estan garantizados en todos los casos ya que dependerá de la gestión de cada persona.</p>

  <script type="text/javascript" src="./js/jquery-3.3.1.min.js"></script>
  <script type="text/javascript" src="./js/popper.min.js"></script>
  <script type="text/javascript" src="./js/bootstrap.min.js"></script>
  <script type="text/javascript" src="./js/mdb.min.js"></script>
  <script src="./js/countdowntime/moment.min.js"></script>
  <script src="./js/countdowntime/moment-timezone.min.js"></script>
  <script src="./js/countdowntime/moment-timezone-with-data.min.js"></script>
  <script src="./js/countdowntime/countdowntime.js"></script>
  <?php
          $dia= date("d");
          $dia = $dia * 1;
          $dia = $dia + 1;
          $mes=date("m");
          ?>
  <script type="text/javascript" src="./js/scripts.js"></script>
  <script>
    $('.cd100').countdown100({
      /*Set Endtime here*/
      /*Endtime must be > current time*/
      endtimeYear: 2018,
      endtimeMonth: <?php echo $mes; ?>,
      endtimeDate: <?php echo $dia; ?>,
      endtimeHours: 0,
      endtimeMinutes: 0,
      endtimeSeconds: 0
      // ex:  timeZone: "America/New_York"
      //go to " http://momentjs.com/timezone/ " to get timezone
    });
  </script>
</body>

</html>
